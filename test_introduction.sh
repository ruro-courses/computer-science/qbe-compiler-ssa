#!/bin/sh
set -euxo pipefail

cd qbe
make
cd ..

mkdir -p obj
qbe/obj/qbe ./introduction.qbe | cc -x assembler -o obj/qbe_introduction -
obj/qbe_introduction > introduction.result


BIN = obj/qbe_ssa
LIB = libqbe.a
INC := qbe

V = @
OBJDIR = qbe/obj

BINSRC   = main_ssa.c stubs.c
SRC      = util.c parse.c cfg.c mem.c ssa.c alias.c load.c \
		   copy.c fold.c live.c spill.c rega.c gas.c
AMD64SRC = amd64/targ.c amd64/sysv.c amd64/isel.c amd64/emit.c
ARM64SRC = arm64/targ.c arm64/abi.c arm64/isel.c arm64/emit.c
SRCALL   = $(SRC) $(AMD64SRC) $(ARM64SRC)

AMD64OBJ = $(AMD64SRC:%.c=$(OBJDIR)/%.o)
ARM64OBJ = $(ARM64SRC:%.c=$(OBJDIR)/%.o)
OBJ      = $(SRC:%.c=$(OBJDIR)/%.o) $(AMD64OBJ) $(ARM64OBJ)

ARFLAGS = $(if $(V),crs,crsv)
CFLAGS += -Wall -Wextra -std=c99 -g -pedantic -I.

all: $(BIN)

$(BIN): $(BINSRC) $(OBJ) $(OBJDIR)/timestamp
	@mkdir -p obj
	@test -z "$(V)" || echo "ld $@"
	$(V)$(CC) $(CFLAGS) $(LDFLAGS) $(BINSRC) $(OBJ) -o $@

clean:
	rm -fr $(BIN)

.PHONY: all clean

import collections
import dataclasses
import os
import sys
import typing


@dataclasses.dataclass
class Var:
    idx: int
    name: str
    source_block: str = None

    def __repr__(self):
        return f'%{self.name} {self.idx}' + (
            "" if self.source_block is None
            else f' (@{self.source_block})'
        )


@dataclasses.dataclass
class Phi:
    to: Var
    args: typing.List[Var]

    def __repr__(self):
        return f"{self.to!r} <-" + (
            '\n    ' if not self.args
            else '\n' + ''.join(
                f"        {a!r}\n"
                for a in self.args
            ) + '    '
        )


@dataclasses.dataclass
class Block:
    name: str
    phi_nodes: typing.List[Phi]

    def __repr__(self):
        return f"@{self.name}\n" + (
            ''.join(f"    {phi!r};\n\n" for phi in self.phi_nodes)
        )


def parse_to(pair):
    var, idx = pair
    return Var(idx=int(idx), name=var)


def parse_arg(to, pair):
    blk, idx = pair
    if blk.startswith('@'):
        blk = blk[1:]
    else:
        raise RuntimeError(
            f"Argument pair {pair} doesn't start with a block name."
        )
    return Var(idx=int(idx), name=to.name, source_block=blk)


def parse_phi(line):
    line = line.split()
    to = parse_to(line[:2])
    if line[2] != '=':
        raise RuntimeError(f"Invalid phi statement {line}.")
    arg_it = iter(line[3:])
    args = tuple(
        parse_arg(to, arg)
        for arg in zip(arg_it, arg_it)
    )
    return Phi(to=to, args=sorted(args, key=lambda a: a.source_block))


def parse_phi_blocks():
    blocks = collections.defaultdict(list)
    cur_block = None

    for line in sys.stdin:
        line = line.strip()
        if line.startswith('@') and line.endswith(':'):
            cur_block = line[1:-1]
            if cur_block in blocks:
                raise RuntimeError(f"Duplicate block {cur_block}.")
            blocks[cur_block] = []
        elif line.startswith('%') and cur_block is not None:
            blocks[cur_block].append(parse_phi(line[1:]))
        else:
            raise RuntimeError(f"Couldn't parse line {line}.")

    return sorted(
        (
            Block(name=name, phi_nodes=sorted(
                phi_nodes,
                key=lambda p: p.to.name
            ))
            for name, phi_nodes in blocks.items()
        ),
        key=lambda b: b.name
    )


def normalize_idx(idx, var, known_vars):
    if idx == -1:
        return idx
    if idx not in known_vars[var]:
        known_vars[var][idx] = len(known_vars[var])
    return known_vars[var][idx]


def normalize_var(var: Var, known_vars):
    var.idx = normalize_idx(var.idx, var.name, known_vars)
    return var


def normalize_phi(phi: Phi, known_vars):
    phi.to = normalize_var(phi.to, known_vars)
    for idx in range(len(phi.args)):
        phi.args[idx] = normalize_var(
            phi.args[idx], known_vars
        )
    return phi


def normalize_blocks(blocks):
    known_vars = collections.defaultdict(dict)
    for block in blocks:
        for idx in range(len(block.phi_nodes)):
            block.phi_nodes[idx] = normalize_phi(
                block.phi_nodes[idx], known_vars
            )


def main():
    solution = parse_phi_blocks()
    normalize_blocks(solution)
    for block in solution:
        print(block)
    return os.EX_OK


if __name__ == '__main__':
    exit(main())

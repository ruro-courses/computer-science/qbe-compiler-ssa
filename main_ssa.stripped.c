#include <qbe/all.h>

#include <stdio.h>

// region Helper macros

#define ITER_RANGE(T, v, start, len, offset)                                   \
T *(v) = &(start)[(offset)];                                                   \
for (                                                                          \
        size_t (v##off) = (offset);                                            \
        (v##off) < (size_t)(len);                                              \
        (v) = &(start)[++(v##off)]                                             \
)

#define LINK_RANGE(T, v, start)                                                \
for (T *v = start; v; v = v->link)

#define GET_VAR(fn, ref)                                                       \
((fn)->tmp[(ref).val])

#define GET_BLK(fn, bid)                                                       \
((fn)->rpo[bid])

#define TRUNC_PTR(ptr)                                                         \
((uint32_t)(int64_t)(ptr))

void set_ref_phi(Ref *ref, int64_t ptr)
{ memcpy(ref, &ptr, sizeof(Ref)); }

int64_t get_ref_phi(Ref *ref)
{
    uint32_t val = 0;
    memcpy(&val, ref, sizeof(val));
    return (val == (uint32_t) -1) ? -1 : (int64_t) val;
}

// endregion

// region WorkLists

typedef struct
{
    Blk **wblk;
    int nwblk;
} WorkList;

Blk **find_block_in_list(WorkList wl, Blk *blk)
{
    ITER_RANGE(Blk *, wb, wl.wblk, wl.nwblk, 0)
    {
        if (*wb == blk)
        { return wb; }
    }
    return NULL;
}

void push_block_to_worklist(WorkList *wl, Blk *blk)
{
    // If this block is not already in the worklist, append it.
    Blk **workblock = find_block_in_list(*wl, blk);
    if (!workblock)
    {
        vgrow(&wl->wblk, ++(wl->nwblk));
        wl->wblk[wl->nwblk - 1] = blk;
    }
}
// endregion

// region SSA Implementation

/// Prints all the blocks in the function fn and all the phi functions in each
/// block in the format specified for the task:
/// %varname target_idx = @blk source_idx (@blk source_idx)+
void print_blkphi(Fn *fn)
{
    ITER_RANGE(Blk *, blk, fn->rpo, fn->nblk, 0)
    {
        printf("@%s:\n", (*blk)->name);
        LINK_RANGE(Phi, phi, (*blk)->phi)
        {
            printf(
                "    %%%s %ld =",
                GET_VAR(fn, phi->to).name,
                get_ref_phi(&phi->arg[0])
            );
            ITER_RANGE(Ref, argvar, phi->arg, phi->narg, 1)
            {
                printf(
                    " @%s %ld",
                    phi->blk[argvar - phi->arg]->name,
                    get_ref_phi(argvar)
                );
            }
            printf("\n");
        }
    }
}

/// Hijack the var->use vector to store not only the uses of the variable, but
/// also its definitions. The final lenght of the (var->use) vector will thus
/// be (var->nuse) + (var->ndef).
void fill_def(Fn *fn)
{
    ITER_RANGE(Tmp, var, fn->tmp, fn->ntmp, Tmp0)
    {
        if (!var->ndef)
        { continue; }
        uint cur_ndef = 0;
        vgrow(&var->use, var->nuse + var->ndef);
        ITER_RANGE(Blk *, blk, fn->rpo, fn->nblk, 0)
        {
            LINK_RANGE(Phi, phi, (*blk)->phi)
            {
                // Technically, we don't need to loop over phi nodes,
                // since they shouldn't appear before the SSA conversion
                // but I still chose to do so just in case.
                if (&GET_VAR(fn, phi->to) == var)
                {
                    var->use[var->nuse + cur_ndef] = (Use) {
                        .bid=(*blk)->id,
                        .type=UPhi,
                        .u={.phi=phi}
                    };
                    cur_ndef++;
                }
            }
            ITER_RANGE(Ins, ins, (*blk)->ins, (*blk)->nins, 0)
            {
                // For every instruction in every block, if the output variable
                // is our current variable, then this is a definition. Remember
                // it, also increment the current ndef.
                if (&GET_VAR(fn, ins->to) == var)
                {
                    var->use[var->nuse + cur_ndef] = (Use) {
                        .bid=(*blk)->id,
                        .type=UIns,
                        .u={.ins=ins}
                    };
                    cur_ndef++;
                }
            }
        }
        assert(cur_ndef == var->ndef);
    }
}

/// Scan the known definitions of the variable (instructions and phi nodes)
/// and recurse over the immediate dominators to find the closest definition,
/// visible from the current block. Return the unique id for the definition.
///
/// The unique id can be stored over a Ref object and should be stored and
/// extracted using set_ref_phi and get_ref_phi.
uint32_t find_def(Fn *fn, Blk *blk, Tmp *var)
{
    // Search definition in current blocks instructions
    ITER_RANGE(Use, var_def, var->use + var->nuse, var->ndef, 0)
    {
        if ((size_t) var_def->bid == blk->id)
        { return TRUNC_PTR(&blk->ins); }
    }

    // Search definition in current blocks phi nodes
    LINK_RANGE(Phi, phi, blk->phi)
    {
        if (&GET_VAR(fn, phi->to) == var)
        { return TRUNC_PTR(&blk->phi); }
    }

    // Search definition in immediate dominator
    if (blk->idom)
    { return find_def(fn, blk->idom, var); }

    // No definition found
    return -1;
}

/// Fill in the phi node references for all the arguments, using find_def to
/// search for the definition of the target variable.
void populate_phi(Fn *fn, Phi *phi)
{
    Tmp *var = &GET_VAR(fn, phi->to);
    ITER_RANGE(Blk *, src, phi->blk, phi->narg, 1)
    { set_ref_phi(&phi->arg[src - phi->blk], find_def(fn, *src, var)); }
}

/// Insert an empty phi node for specified variable into specified block.
/// Do nothing, if a phi node for this var already exists. Note that unlike QBE,
/// I store the target variable information in phi->to and in phi->arg[0].
/// (phi->arg[i] contains the phi index of each argument and arg[0] == target)
void insert_phi(Fn *fn, Blk *blk, Tmp *var)
{
    Phi **target;
    for (target = &blk->phi; *target; target = &(*target)->link)
    {
        // Phi node for current variable already exists in blk
        if (&GET_VAR(fn, (*target)->to) == var)
        { return; }
    }

    // Phi node for current variable doesn't exist,
    // we need to create a new phi instruction
    Phi *phi = alloc(sizeof(Phi));
    phi->narg = 1 + blk->npred;
    phi->to = (Ref) {.type=RTmp, .val=var - fn->tmp};
    phi->blk[0] = NULL;
    set_ref_phi(&phi->arg[0], (int64_t) &blk->phi);
    ITER_RANGE(Blk *, pred, blk->pred, blk->npred, 0)
    {
        phi->blk[1 + pred - blk->pred] = *pred;
        set_ref_phi(&phi->arg[1 + pred - blk->pred], -1);
    }
    *target = phi;
}

/// Analyze the function and output the semi-pruned SSA description for it.
/// The liveness of the variables is not taken into account and it is assumed
/// that all the variables are global (used in more than 1 block)
static void simple_ssa(Fn *fn)
{
    fillrpo(fn);
    filluse(fn);
    filldom(fn);
    fillfron(fn);

    // fill_def is the rough equivalent to Blocks(x) from the slides,
    // however it appends the definitions to the var->use vector.
    fill_def(fn);

    // For each variable
    ITER_RANGE(Tmp, var, fn->tmp, fn->ntmp, Tmp0)
    {
        // Initialize worklist.
        WorkList wl = {
            .wblk=vnew(0, sizeof(Blk *), Pfn),
            .nwblk=0
        };

        // For each definition of the variable
        ITER_RANGE(Use, var_use, var->use + var->nuse, var->ndef, 0)
        {
            // Add the containing block to the worklist
            push_block_to_worklist(&wl, GET_BLK(fn, var_use->bid));
        }

        // For each block currently in the worklist
        ITER_RANGE(Blk *, blk, wl.wblk, wl.nwblk, 0)
        {
            // For each block in its dominance frontier
            ITER_RANGE(Blk *, fblk, (*blk)->fron, (*blk)->nfron, 0)
            {
                // Insert a phi node into the frontier block
                insert_phi(fn, *fblk, var);
                // Add the block to the worklist (since phi is a definition)
                push_block_to_worklist(&wl, *fblk);
            }
        }
        vfree(wl.wblk);
    }

    ITER_RANGE(Blk *, blk, fn->rpo, fn->nblk, 0)
    {
        LINK_RANGE(Phi, phi, (*blk)->phi)
        { populate_phi(fn, phi); }
    }

    // Print resulting phi nodes
    print_blkphi(fn);
}
// endregion

// region API

static void readfn(Fn *fn)
{ simple_ssa(fn); }

static void readdat(Dat *dat)
{ (void) dat; }

int main(void)
{
    parse(stdin, "<stdin>", readdat, readfn);
    freeall();
}
// endregion

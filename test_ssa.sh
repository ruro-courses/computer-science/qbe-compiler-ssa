#!/bin/bash
set -uo pipefail
set -e

cd qbe
make
cd ..
make

echo -ne "\n\n"
find ssa_tests -type f -printf "%P\n" | sort | while read -r f;
do
    obj/qbe_ssa <"ssa_tests/${f}" >"ssa_outputs/${f}.solution"

    set +e
    DIFF=$(                                               \
        diff -y --color=always                            \
        <(python reorder.py <"ssa_outputs/${f}.solution") \
        <(python reorder.py <"ssa_outputs/${f}.correct")  \
    )
    DIFF_STATUS=$?
    set -e

    echo -n "${f}"
    ((DIFF_STATUS)) && echo -n " [FAIL]" || echo -n " [OK]"
    echo -ne "\n"
    ((DIFF_STATUS)) && echo -ne "---- failed diff ----\n\n${DIFF}\n\n---------------------\n"
done

#include <qbe/all.h>

#include <stdio.h>

// region Helper macros

static inline void __attribute__((unused))
_UNUSED(int dummy, ...)
{ (void) dummy; }

#define DEBUG
#ifdef DEBUG
    #define DEBUG_FD stderr
#endif

#ifdef DEBUG_FD
    #define DPRINT(...) fprintf(DEBUG_FD, __VA_ARGS__)
#else
    #define DPRINT(...) _UNUSED(0, __VA_ARGS__)
#endif

#define ITER_RANGE(T, v, start, len, offset)                                   \
T *(v) = &(start)[(offset)];                                                   \
for (                                                                          \
        size_t (v##off) = (offset);                                            \
        (v##off) < (size_t)(len);                                              \
        (v) = &(start)[++(v##off)]                                             \
)

#define LINK_RANGE(T, v, start)                                                \
for (T *v = start; v; v = v->link)

#define IS_NAMED(ref)                                                          \
((ref).type == RTmp && Tmp0 <= (ref).val)

#define GET_VAR(fn, ref)                                                       \
((fn)->tmp[(ref).val])

#define GET_BLK(fn, bid)                                                       \
((fn)->rpo[bid])

#define VAR_NAME(fn, ref)                                                      \
(IS_NAMED(ref) ? GET_VAR(fn, ref).name : "?")

#define BLK_NAME(fn, bid)                                                      \
(GET_BLK(fn, bid)->name)

#define INS_NAME(ins)                                                          \
(optab[(ins)->op].name)

#define TRUNC_PTR(ptr)                                                         \
((uint32_t)(int64_t)(ptr))

void set_ref_phi(Ref *ref, int64_t ptr)
{ memcpy(ref, &ptr, sizeof(Ref)); }

int64_t get_ref_phi(Ref *ref)
{
    uint32_t val = 0;
    memcpy(&val, ref, sizeof(val));
    return (val == (uint32_t) -1) ? -1 : (int64_t) val;
}
// endregion

// region WorkLists

typedef struct
{
    Blk **wblk;
    int nwblk;
} WorkList;

Blk **find_block_in_list(WorkList wl, Blk *blk)
{
    ITER_RANGE(Blk *, wb, wl.wblk, wl.nwblk, 0)
    {
        if (*wb == blk)
        {
            DPRINT("Found block @%s in worklist.\n", (*wb)->name);
            return wb;
        }
    }
    DPRINT("Didn't find @%s in worklist.\n", blk->name);
    return NULL;
}

void dprint_worklist(WorkList *wl)
{
    DPRINT("worklist: [");
    ITER_RANGE(Blk *, _blk, wl->wblk, wl->nwblk, 0)
    {
        DPRINT(" @%s", (*_blk)->name);
    }
    DPRINT(" ]\n");
}

void push_block_to_worklist(WorkList *wl, Blk *blk)
{
    assert(blk != NULL);
    // If this block is not already in the worklist, append it.
    DPRINT("      ");
    Blk **workblock = find_block_in_list(*wl, blk);
    if (!workblock)
    {
        DPRINT("      Appending to worklist.\n");
        vgrow(&wl->wblk, ++(wl->nwblk));
        wl->wblk[wl->nwblk - 1] = blk;
    }
    DPRINT("      New ");
    dprint_worklist(wl);
}
// endregion

// region Debug prints

void dprint_ins(Fn *fn, Ins *ins)
{
    DPRINT(
        "    %%%s = %s %%%s, %%%s\n",
        VAR_NAME(fn, ins->to),
        INS_NAME(ins),
        VAR_NAME(fn, ins->arg[0]),
        VAR_NAME(fn, ins->arg[1])
    );
}

void dprint_phi(Fn *fn, Phi *phi)
{
    DPRINT(
        "    %%%s %ld =",
        VAR_NAME(fn, phi->to),
        get_ref_phi(&phi->arg[0])
    );
    ITER_RANGE(Ref, argvar, phi->arg, phi->narg, 1)
    {
        DPRINT(
            " @%s %ld",
            phi->blk[argvar - phi->arg]->name,
            get_ref_phi(argvar)
        );
    }
    DPRINT("\n");
}

void dprint_var(Fn *fn, Tmp *var)
{
    if (var->phi)
    {
        DPRINT(
            "  %%%s is an ssa alias for %%%s\n",
            var->name, fn->tmp[var->phi].name
        );
    }
    DPRINT("  %%%s Used in blocks:\n", var->name);
    ITER_RANGE(Use, usage, var->use, var->nuse, 0)
    {
        DPRINT("    @%s", BLK_NAME(fn, usage->bid));
        switch (usage->type)
        {
            case UIns:dprint_ins(fn, usage->u.ins);
                break;
            case UJmp:DPRINT("    (jmp)\n");
                break;
            case UPhi:dprint_phi(fn, usage->u.phi);
                break;
            case UXXX:DPRINT("    (XXX)\n");
                break;
        }
    }
    DPRINT("  %%%s Defined in blocks:\n", var->name);
    ITER_RANGE(Use, definition, var->use + var->nuse, var->ndef, 0)
    {
        DPRINT("    @%s", BLK_NAME(fn, definition->bid));
        switch (definition->type)
        {
            case UIns:dprint_ins(fn, definition->u.ins);
                break;
            case UJmp:DPRINT("    (jmp)\n");
                break;
            case UPhi:dprint_phi(fn, definition->u.phi);
                break;
            case UXXX:DPRINT("    (XXX)\n");
                break;
        }
    }
    DPRINT("\n");
}

void dprint_bset(Fn *fn, BSet *bs)
{
    DPRINT("[");
    for (int t = Tmp0; bsiter(bs, &t); t++)
    { DPRINT(" %s", fn->tmp[t].name); }
    DPRINT(" ]\n");
}

void dprint_blk(Fn *fn, Blk *blk)
{
    DPRINT("  @%s:\n", blk->name);

    // Dominators
    if (blk->idom)
    { DPRINT("    Is dominated by: @%s\n", blk->idom->name); }
    if (blk->dom)
    { DPRINT("    Dominates over: @%s\n", blk->dom->name); }

    // Liveness
    DPRINT("    Liveness:\n");
    DPRINT("      in:");
    dprint_bset(fn, blk->in);
    DPRINT("      out:");
    dprint_bset(fn, blk->out);
    DPRINT("      gen:");
    dprint_bset(fn, blk->gen);
    DPRINT("      live: %d %d\n", blk->nlive[0], blk->nlive[1]);

    // Frontier
    DPRINT("    Dominance frontier:\n");
    ITER_RANGE(Blk *, fblk, blk->fron, blk->nfron, 0)
    {
        DPRINT("      @%s\n", (*fblk)->name);
    }

    // Iterate over predecessors
    DPRINT("    Predecessors:\n");
    ITER_RANGE(Blk *, pblk, blk->pred, blk->npred, 0)
    {
        DPRINT("      @%s\n", (*pblk)->name);
    }

    // Iterate over phi nodes
    DPRINT("    Phi nodes:\n");
    LINK_RANGE(Phi, phi, blk->phi)
    {
        DPRINT("  ");
        dprint_phi(fn, phi);
    }

    // Iterate over instructions
    DPRINT("    Instructions:\n");
    ITER_RANGE(Ins, instr, blk->ins, blk->nins, 0)
    {
        // Ignore instructions that have no named out parameter
        if (!IS_NAMED(instr->to))
        { continue; }

        DPRINT("  ");
        dprint_ins(fn, instr);
    }
    DPRINT("\n");
}
// endregion

// region SSA Implementation

/// Prints all the blocks in the function fn and all the phi functions in each
/// block in the format specified for the task:
/// %varname target_idx = @blk source_idx (@blk source_idx)+
void print_blkphi(Fn *fn)
{
    ITER_RANGE(Blk *, blk, fn->rpo, fn->nblk, 0)
    {
        printf("@%s:\n", (*blk)->name);
        LINK_RANGE(Phi, phi, (*blk)->phi)
        {
            printf(
                "    %%%s %ld =",
                VAR_NAME(fn, phi->to),
                get_ref_phi(&phi->arg[0])
            );
            ITER_RANGE(Ref, argvar, phi->arg, phi->narg, 1)
            {
                printf(
                    " @%s %ld",
                    phi->blk[argvar - phi->arg]->name,
                    get_ref_phi(argvar)
                );
            }
            printf("\n");
        }
    }
}

/// Hijack the var->use vector to store not only the uses of the variable, but
/// also its definitions. The final lenght of the (var->use) vector will thus
/// be (var->nuse) + (var->ndef).
void fill_def(Fn *fn)
{
    ITER_RANGE(Tmp, var, fn->tmp, fn->ntmp, Tmp0)
    {
        if (!var->ndef)
        { continue; }
        uint cur_ndef = 0;
        vgrow(&var->use, var->nuse + var->ndef);
        ITER_RANGE(Blk *, blk, fn->rpo, fn->nblk, 0)
        {
            LINK_RANGE(Phi, phi, (*blk)->phi)
            {
                // Technically, we don't need to loop over phi nodes,
                // since they shouldn't appear before the SSA conversion
                // but I still chose to do so just in case.
                if (&GET_VAR(fn, phi->to) == var)
                {
                    var->use[var->nuse + cur_ndef] = (Use) {
                        .bid=(*blk)->id,
                        .type=UPhi,
                        .u={.phi=phi}
                    };
                    cur_ndef++;
                }
            }
            ITER_RANGE(Ins, ins, (*blk)->ins, (*blk)->nins, 0)
            {
                // For every instruction in every block, if the output variable
                // is our current variable, then this is a definition. Remember
                // it, also increment the current ndef.
                if (&GET_VAR(fn, ins->to) == var)
                {
                    var->use[var->nuse + cur_ndef] = (Use) {
                        .bid=(*blk)->id,
                        .type=UIns,
                        .u={.ins=ins}
                    };
                    cur_ndef++;
                }
            }
        }
        assert(cur_ndef == var->ndef);
    }
}

/// Scan the known definitions of the variable (instructions and phi nodes)
/// and recurse over the immediate dominators to find the closest definition,
/// visible from the current block. Return the unique id for the definition.
///
/// The unique id can be stored over a Ref object and should be stored and
/// extracted using set_ref_phi and get_ref_phi.
uint32_t find_def(Fn *fn, Blk *blk, Tmp *var)
{
    // Search definition in current blocks instructions
    DPRINT("Searching instructions of @%s.\n", blk->name);
    ITER_RANGE(Use, var_def, var->use + var->nuse, var->ndef, 0)
    {
        if ((size_t) var_def->bid == blk->id)
        {
            DPRINT("Found in ins %u.\n", TRUNC_PTR(&blk->ins));
            return TRUNC_PTR(&blk->ins);
        }
    }

    // Search definition in current blocks phi nodes
    DPRINT("Searching phi nodes of @%s.\n", blk->name);
    LINK_RANGE(Phi, phi, blk->phi)
    {
        if (&GET_VAR(fn, phi->to) == var)
        {
            DPRINT("Found in phi %u.\n", TRUNC_PTR(&blk->phi));
            return TRUNC_PTR(&blk->phi);
        }
    }

    // Search definition in immediate dominator
    if (blk->idom)
    {
        DPRINT("Searching dominator @%s\n", blk->idom->name);
        return find_def(fn, blk->idom, var);
    }

    // No definition found
    DPRINT("No definitions found for %%%s in @%s.\n", var->name, blk->name);
    return -1;
}

/// Fill in the phi node references for all the arguments, using find_def to
/// search for the definition of the target variable.
void populate_phi(Fn *fn, Phi *phi)
{
    Tmp *var = &GET_VAR(fn, phi->to);
    ITER_RANGE(Blk *, src, phi->blk, phi->narg, 1)
    {
        DPRINT("Populating arg[%ld]:", src - phi->blk);
        dprint_phi(fn, phi);
        set_ref_phi(&phi->arg[src - phi->blk], find_def(fn, *src, var));
        DPRINT("Result:    ");
        dprint_phi(fn, phi);
    }
}

/// Insert an empty phi node for specified variable into specified block.
/// Do nothing, if a phi node for this var already exists. Note that unlike QBE,
/// I store the target variable information in phi->to and in phi->arg[0].
/// (phi->arg[i] contains the phi index of each argument and arg[0] == target)
void insert_phi(Fn *fn, Blk *blk, Tmp *var)
{
    Phi **target;
    for (target = &blk->phi; *target; target = &(*target)->link)
    {
        if (&GET_VAR(fn, (*target)->to) == var)
        {
            // Phi node for current variable already exists in blk
            DPRINT("Skipping phi node generation for:\n");
            DPRINT("  blk: @%s\n", blk->name);
            DPRINT("  var: %%%s\n", var->name);
            return;
        }
    }

    // Phi node for current variable doesn't exist,
    // we need to create a new phi instruction
    DPRINT("Generating phi node for:\n");
    DPRINT("  blk: @%s\n", blk->name);
    DPRINT("  var: %%%s\n", var->name);
    Phi *phi = alloc(sizeof(Phi));
    phi->narg = 1 + blk->npred;
    phi->to = (Ref) {.type=RTmp, .val=var - fn->tmp};
    phi->blk[0] = NULL;
    set_ref_phi(&phi->arg[0], (int64_t) &blk->phi);
    ITER_RANGE(Blk *, pred, blk->pred, blk->npred, 0)
    {
        phi->blk[1 + pred - blk->pred] = *pred;
        set_ref_phi(&phi->arg[1 + pred - blk->pred], -1);
    }
    *target = phi;
}

/// Analyze the function and output the semi-pruned SSA description for it.
/// The liveness of the variables is not taken into account and it is assumed
/// that all the variables are global (used in more than 1 block)
static void simple_ssa(Fn *fn)
{
    fillrpo(fn);
    filluse(fn);
    filldom(fn);
    fillfron(fn);

    // fill_def is the rough equivalent to Blocks(x) from the slides,
    // however it appends the definitions to the var->use vector.
    fill_def(fn);

    DPRINT("Temporaries:\n");
    ITER_RANGE(Tmp, _var, fn->tmp, fn->ntmp, Tmp0)
    { dprint_var(fn, _var); }

    DPRINT("Blocks (Before):\n");
    ITER_RANGE(Blk *, _blk_rpo1, fn->rpo, fn->nblk, 0)
    { dprint_blk(fn, *_blk_rpo1); }

    // For each variable
    ITER_RANGE(Tmp, var, fn->tmp, fn->ntmp, Tmp0)
    {
        // Initialize worklist.
        WorkList wl = {
            .wblk=vnew(0, sizeof(Blk *), Pfn),
            .nwblk=0
        };

        DPRINT("Processing %%%s.\n", var->name);
        // For each definition of the variable
        ITER_RANGE(Use, var_use, var->use + var->nuse, var->ndef, 0)
        {
            DPRINT("  Pushing block @%s.\n", BLK_NAME(fn, var_use->bid));
            // Add the containing block to the worklist
            push_block_to_worklist(&wl, GET_BLK(fn, var_use->bid));
        }
        DPRINT("\n");

        // For each block currently in the worklist
        ITER_RANGE(Blk *, blk, wl.wblk, wl.nwblk, 0)
        {
            DPRINT("  Current ");
            dprint_worklist(&wl);
            assert(*blk != NULL);
            DPRINT("  Processing block @%s.\n", (*blk)->name);
            fflush(stdout);
            // For each block in its dominance frontier
            ITER_RANGE(Blk *, fblk, (*blk)->fron, (*blk)->nfron, 0)
            {
                // Insert a phi node into the frontier block
                DPRINT("    Processing it's frontier @%s.\n", (*fblk)->name);
                insert_phi(fn, *fblk, var);
                DPRINT("    Pushing frontier block @%s.\n", (*fblk)->name);
                // Add the block to the worklist (since phi is a definition)
                push_block_to_worklist(&wl, *fblk);
            }
            DPRINT("\n");
        }
        vfree(wl.wblk);
    }

    // Fill the phi indices for all the phi nodes
    ITER_RANGE(Blk *, blk, fn->rpo, fn->nblk, 0)
    {
        LINK_RANGE(Phi, phi, (*blk)->phi)
        { populate_phi(fn, phi); }
    }

    DPRINT("Blocks (After):\n");
    ITER_RANGE(Blk *, _blk_rpo2, fn->rpo, fn->nblk, 0)
    { dprint_blk(fn, *_blk_rpo2); }

    // Print resulting phi nodes
    print_blkphi(fn);
}
// endregion

// region API

static void readfn(Fn *fn)
{ simple_ssa(fn); }

static void readdat(Dat *dat)
{ (void) dat; }

int main(void)
{
    parse(stdin, "<stdin>", readdat, readfn);
    freeall();
}
// endregion
